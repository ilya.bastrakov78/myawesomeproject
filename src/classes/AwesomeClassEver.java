package classes;

public class AwesomeClassEver {
    public static void main(String[] args) {
        JustClass justClass = new JustClass();
        SampleClass sampleClass = new SampleClass();
        System.out.println("Hello Worlds!");
        justClass.greetingFromJustClass();
        sampleClass.greetingSampleClass();

        System.out.println("Hm, DEJAVU");
    }
}
